package main

import (
	"encoding/xml"
	"fmt"
	"strconv"
	"time"
)

type Pack struct {
	XMLName    xml.Name  `xml:"package"`
	Name       string    `xml:"name,attr"`
	Id         string    `xml:"id,attr"`
	Date       time.Time `xml:"date,attr"`
	Difficulty string    `xml:"difficulty,attr"`
	Xmlns      string    `xml:"xmlns,attr"`
	Authors    []*Author `xml:"info>authors>author"`
	Rounds     []*Round  `xml:"rounds>round"`
}

type Author struct {
	Name string `xml:"name,attr"`
}

type Round struct {
	Name   string   `xml:"name,attr"`
	Themes []*Theme `xml:"themes>theme"`
}

type Theme struct {
	Name      string      `xml:"name,attr"`
	Questions []*Question `xml:"questions>question"`
}

type Question struct {
	Price    string `xml:"price,attr"`
	Scenario []Atom `xml:"scenario>atom"`
	Answer   string `xml:"right>answer"`
}

type Atom struct {
	Type string `xml:"type,attr,omitempty"`
	Text string `xml:",chardata"`
}

func CreatePack(name string) *Pack {
	return &Pack{
		Name: name,
		Id:   "",
	}
}

func (p *Pack) AddRound(name string) *Round {
	round := &Round{
		Name: name,
	}
	p.Rounds = append(p.Rounds, round)
	return round
}

func (r *Round) AddTheme(name string) *Theme {
	theme := &Theme{
		Name: name,
	}
	r.Themes = append(r.Themes, theme)
	return theme
}

func (t *Theme) AddQuestion(price int) *Question {
	q := &Question{
		Price: strconv.Itoa(price),
	}
	t.Questions = append(t.Questions, q)
	return q
}

func (q *Question) AddText(text string) {
	q.Scenario = append(q.Scenario, Atom{Text: text})
}

func (q *Question) AddPicture(picture string) {
	q.Scenario = append(q.Scenario, Atom{Type: "marker"}, Atom{Type: "image", Text: picture})
}

func (q *Question) AddSong(song string) {
	q.Scenario = append(q.Scenario, Atom{Type: "voice", Text: song})
}

func (q *Question) SetAnswer(answer string) {
	q.Answer = answer
}

func StartCreating() {
	atom1 := Atom{
		Text: "Когда дрочешь что бормочешь?",
	}
	atom2 := Atom{
		Type: "marker",
	}
	atom3 := Atom{
		Type: "image",
		Text: "@4_1.jpg",
	}
	question := Question{
		Price:    "100",
		Scenario: [](Atom){atom1, atom2, atom3},
		Answer:   "Не дрочу не бормочу",
	}
	theme := Theme{
		Name:      "Йоба",
		Questions: [](*Question){&question},
	}
	round := Round{
		Name:   "Первый блин",
		Themes: [](*Theme){&theme},
	}
	author := Author{
		Name: "Сокол",
	}
	pack := Pack{
		Name:       "ебучи пак",
		Id:         "95b42e93-16c5-4ce4-949b-229c6fd3e6b3",
		Date:       time.Now(),
		Difficulty: "5",
		Xmlns:      "http://ur-quan1986.narod.ru/ygpackage3.0.xsd",
		Authors:    [](*Author){&author},
		Rounds:     [](*Round){&round},
	}

	res, err := xml.Marshal(pack)
	if err != nil {
		fmt.Println("Error:", err.Error())
		return
	}

	fmt.Println(string(res))
}
