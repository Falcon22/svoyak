package main

import (
	"encoding/xml"
	"fmt"
	"strconv"
)

func main() {
	pack := CreatePack("mypack")
	for i := 0; i < 5; i++ {
		r := pack.AddRound(strconv.Itoa(i) + " РАУНД")
		for j := 0; j < 5; j++ {
			t := r.AddTheme(strconv.Itoa(j) + " ТЕМА")
			for k := 0; k < 5; k++ {
				q := t.AddQuestion(k*50 + 50)
				q.AddText("Да")
				q.AddPicture("@1_1_1.jpg")
				q.AddSong("@1_1_1.mp3")
				q.SetAnswer("Пизда")
			}
		}
	}

	res, err := xml.Marshal(pack)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	result := xml.Header + string(res)
	fmt.Println(result)
}
